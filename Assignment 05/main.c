#include <stdio.h>
#include <stdlib.h>
int Attendees(int ticketprice)
{
    return 120 + (20 * (15 - ticketprice)/5);
}

int revenue(int ticketprice)
{
    return Attendees(ticketprice) * ticketprice;
}

int cost(int ticketprice)
{
    return 500 + (3 * Attendees(ticketprice));
}

int profit(int ticketprice)
{
    return revenue(ticketprice)-cost(ticketprice);
}

int main()
{
    int ticketprice, bestprice, maxprofit=0;
    printf("Ticket price\t No. of Attendees\t Profit\n");
    for (ticketprice=0; ticketprice<50; ticketprice = ticketprice + 5)
    {
        printf("Rs.%d\t\t %d\t\t\t %d\n",ticketprice, Attendees(ticketprice), profit(ticketprice));
    }
    for (ticketprice=0; ticketprice<50; ticketprice = ticketprice + 5)
    {
        if (maxprofit < profit(ticketprice))
        {
            maxprofit = profit(ticketprice);
            bestprice = ticketprice;
        }
    }
    printf("\nThe most profitable ticket price is Rs.%d.", bestprice);
    return 0;
}
