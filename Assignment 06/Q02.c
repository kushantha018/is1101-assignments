#include <stdio.h>
#include <stdlib.h>

int fib(int n)
{
    if(n == 0)
        return 0;
    else if(n == 1)
        return 1;
    else
        return fib(n - 1) + fib(n - 2);
}

int pattern(int n)
{
    if(n > 0)
    {
        printf("%d", pattern(n - 1));
        printf("\n");
        fib(n);
    }
    else
        return 0;
}
int main()
{
    int num,a;
    printf("Enter the number : ");
    scanf("%d", &num);
    a = pattern(num);
    printf("%d", a);
    return 0;
}
