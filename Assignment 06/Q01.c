#include <stdio.h>
#include <stdlib.h>

void pattern(int n)
{

    if(n>0)
    {
        printf("%d",n);
        pattern(n - 1);
    }

}

void rows(int n)
{
    if(n>0)
    {
        rows(n - 1);
        printf("\n");
        pattern(n);
    }

}

int main()
{
    int num;
    printf("Enter the number : ");
    scanf("%d",&num);
    rows(num);
    return 0;
}
