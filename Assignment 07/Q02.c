#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_SIZE 100

int main()
{
    int len, i, count = 0;
    char ch;
    char str[MAX_SIZE];
    printf("Enter the sentence :\n");
    fgets(str, MAX_SIZE, stdin);
    printf("Character : ");
    scanf("%ch", &ch);
    len = strlen(str);

    for(i = 0; i < len; i++)
    {
        if(str[i] == ch)
            count = count + 1;
    }
    printf("The frequency of the %c is %d.", ch, count);
    return 0;
}
