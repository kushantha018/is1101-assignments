#include <stdio.h>
#include <stdlib.h>
#define MAX 50

int main()
{
    int a[MAX][MAX], b[MAX][MAX], product[MAX][MAX];
    int arows, acols, brows, bcols;
    int i, j, k;
    int sum = 0;
    printf("If the matrix is 3 * 3, enter 3 3.\n\n");
    printf("Enter the rows and columns of the 1st matrix: ");
    scanf("%d %d", &arows, &acols);

    printf("\nEnter the rows and columns of the 2nd matrix: ");
    scanf("%d %d", &brows, &bcols);

    if((arows == brows)&&(acols == bcols))
    {
        printf("\nEnter the elements of the 1st matrix :\n");
        for(i = 0; i < arows; i++)
        {
            for(j = 0; j < acols; j++)
            {
                scanf("%d", &a[i][j]);
            }
        }

        printf("\nEnter the elements of 2nd the matrix :\n");
        for(i = 0; i < brows; i++)
        {
            for(j = 0; j < bcols; j++)
            {
                scanf("%d", &b[i][j]);
            }
        }

        printf("\n");
        for(i = 0; i < arows; i++)
        {
            for(j = 0; j < bcols; j++)
            {
                for(k = 0; k < brows; k++)
                {
                    sum = sum + (a[i][k] * b[k][j]);
                }
                product[i][j] = sum;
                sum = 0;
            }
        }

        printf("Result of the Multiplication :\n");
        for(i = 0; i < arows; i++)
        {
            for(j = 0; j < bcols; j++)
            {
                printf("%d ", product[i][j]);
            }
            printf("\n");
        }

        printf("\n");
        for(i = 0; i < arows; i++)
        {
            for(j = 0; j < bcols; j++)
            {
                product[i][j] = a[i][j] + b[i][j];
            }
        }

        printf("Result of the Addition :\n");
        for(i = 0; i < arows; i++)
        {
            for(j = 0; j < bcols; j++)
            {
                printf("%d ", product[i][j]);
            }
            printf("\n");
        }
    }
    else
        printf("\nMatrices cannot be added, if the rows and the columns of the both matrices are not equal.\n");
    return 0;
}
