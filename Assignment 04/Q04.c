#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, num;
    printf("Enter a number : ");
    scanf("%d", &num);
    printf("All factors of %d: \n", num);

    for(i=1; i<=num; i = i + 1)
    {
        if(num % i == 0)
        {
            printf("%d\n",i);
        }
    }
    return 0;
}
